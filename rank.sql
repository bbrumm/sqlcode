SELECT * FROM student ORDER BY fees_paid;

/*alter table student add gender varchar2(1);
update student
set gender = 'M'
where first_name in (
'Steven',
'Mark',
'John',
'Tom',
'Andrew',
'Robert'
);

update student
set gender = 'F'
where first_name in (
'Tanya',
'Julie',
'Michelle',
'Susan'
);
*/

--1 Basic
SELECT 
RANK(200) WITHIN GROUP (ORDER BY fees_paid) AS rank_val
FROM student;

--2 Different value, duplicate
SELECT 
RANK(150) WITHIN GROUP (ORDER BY fees_paid) AS rank_val
FROM student;

--3 Different value, duplicate, but second column
SELECT 
RANK(150, 'Minson') WITHIN GROUP (ORDER BY fees_paid, last_name) AS rank_val
FROM student;

--4 Nulls first
SELECT 
RANK(150) WITHIN GROUP (ORDER BY fees_paid NULLS FIRST) AS rank_val
FROM student;

--5 Name but order by value - error
SELECT 
RANK('Tom') WITHIN GROUP (ORDER BY fees_paid) AS rank_val
FROM student;

--6 Name sorting
SELECT 
RANK('Steven') WITHIN GROUP (ORDER BY first_name) AS rank_val
FROM student;


--7 Name that does not exist
SELECT 
RANK('Brad') WITHIN GROUP (ORDER BY first_name) AS rank_val
FROM student;

--Analytical Functions

--8 Basic - partition by gender
SELECT
student_id, first_name, last_name, gender, fees_paid,
RANK() OVER (PARTITION BY gender ORDER BY fees_paid) AS rank_val
FROM student;


--9 Basic - partition by fees paid
SELECT
student_id, first_name, last_name, gender, fees_paid,
RANK() OVER (PARTITION BY fees_paid ORDER BY last_name, first_name) AS rank_val
FROM student;

--10 No partition
SELECT
student_id, first_name, last_name, gender, fees_paid,
RANK() OVER (ORDER BY fees_paid) AS rank_val
FROM student;
